#from app import app

from flask import Flask

app = Flask(__name__)


@app.route('/')
def main():
    return "Hello this is maze gen base"


# this is required by openshift because the run command executes it directly
if __name__ == '__main__':  # Script executed directly?
    print("Hello World! Built with a Docker file.")
    app.run(host="0.0.0.0", port=5000, debug=True,use_reloader=True)  # Launch built-in web server and run this Flask webapp

